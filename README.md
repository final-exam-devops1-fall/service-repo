# Final Assignment Infrastructure


# Project Name
Thrawn

## Short Description
This project was created specifically for the DevOps course

## Detailed Description
Home Page:

The home page features a clean and engaging layout with a navigation bar, search functionality, and rotating banners showcasing ongoing deals and popular products.
Acts as the landing page for users, providing them with immediate access to product categories, special offers, and search capabilities.

Product Listing and Filters:

A dynamic listing of products with multiple filter options like category, price range, brand, etc.
Allows users to browse through products efficiently and narrow down their search based on specific requirements.

Product Detail Page:

Each product has a detailed page with high-resolution images, descriptions, price, availability, and customer reviews.
Offers comprehensive information about the product to assist customers in making informed purchasing decisions.

Shopping Cart:

A feature where users can add products for checkout. It includes options for quantity adjustment and removing items.
Facilitates the purchasing process by allowing users to review and modify their selected items before proceeding to payment.

User Account Dashboard:

A personal area for registered users to manage their profiles, view order history, and track current orders.
Enhances user experience by providing a personalized space for order management and personal data editing.

SEO and Analytics:

The website is SEO optimized and integrated with analytics tools for tracking user behavior and website performance.
Helps in improving website visibility in search engines and provides insights for data-driven decision-making.

## Responsible People
- 21B030692 - Korganbek Dinmukhammed Almasuly: Responsible for Infrastructure
- 21B030684 - Kakimov Aidos: Responsible for Interaction with infrastructure
- 21B031186 - Tugelbay Ansar Temirbekuly: Responsible for Logging and Monitoring for your system
- 21В030705 Oralbay Nurzhan Kuanyshbaiuly: Responsible for Preparation for work(pre-setup)

## INSTALL Section
Step 1: Clone the Repository from GitLab
First, you need to clone the project repository from GitLab. Open your terminal or command prompt and use the following command:
git clone https://gitlab.com/final-exam-devops1-fall/infrastructure-repo/;

Step 2: Navigate to the Project Directory
After cloning the repository, navigate to the project directory:
cd infrastructure-repo

Step 3: Docker Compose

docker-compose up -d

Step 4: Check Containers

Verify that the Docker containers are running:

docker ps

This command lists all running Docker containers. You should see the containers for your project listed.

Step 5: Set Up GitLab CI/CD

Ensure the .gitlab-ci.yml file is set up according to your CI/CD pipeline needs.

Push your changes to GitLab to trigger the CI/CD pipeline:

git add .
git commit -m "Set up CI/CD"
git push origin master

If you want to contribute to the project you can contact us via emails:

ai_kakimov@kbtu.kz
a_tugelbai@kbtu.kz
d_korganbek@kbtu.kz
n_oralbai@kbtu.kz

Or you can also contact via github accounts:

@qorganbek
@victus-est-deus
@ansartugelbay 
@akakimov 
