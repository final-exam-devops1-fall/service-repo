package handlers

import (
	"assignment3/src/initializers"
	"assignment3/src/models"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func ShowMainPage(c *gin.Context) {

	var products []models.Product

	tokenString, err := c.Cookie("token")
	if err != nil {
		c.HTML(http.StatusOK, "mainpage.html", gin.H{
			"Products":        products,
			"IsAuthenticated": false,
		})
		return
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("unexpected signing method")
		}

		return []byte("secret"), nil
	})
	if err != nil {
		c.HTML(http.StatusOK, "mainpage.html", gin.H{
			"Products":        products,
			"IsAuthenticated": false,
		})
		return
	}

	if _, ok := token.Claims.(jwt.MapClaims); !ok && !token.Valid {
		c.HTML(http.StatusOK, "mainpage.html", gin.H{
			"Products":        products,
			"IsAuthenticated": false,
		})
		return
	}

	initializers.DB.Find(&products)

	c.HTML(http.StatusOK, "mainpage.html", gin.H{
		"Products":        products,
		"IsAuthenticated": true,
	})
}

func GetAllProducts(c *gin.Context) {
	var products []models.Product
	err := initializers.DB.Find(&products).Error
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": products})
}
func CreateProductHandler(c *gin.Context) {
	var product models.Product
	if err := c.ShouldBindJSON(&product); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err := initializers.DB.Create(&product).Error
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusCreated, gin.H{"data": product})
}

func GetUserIDFromToken(c *gin.Context) (uint, error) {
	tokenString, err := c.Cookie("token")
	if err != nil {
		return 0, err
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte("your-secret-key"), nil
	})
	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return 0, errors.New("invalid token")
	}

	userID, ok := claims["user_id"].(float64)
	if !ok {
		return 0, errors.New("user ID not found in token claims")
	}

	fmt.Println("User ID:", uint(userID))

	return uint(userID), nil
}

func SetNewOrderID(c *gin.Context) {

	userId, err := c.Cookie("user_id")

	if err != nil {
		c.JSON(404, gin.H{"message": "Error when collect user_id from cookie!"})
		return
	}

	var order models.Order

	initializers.DB.Where("user_id = ? AND status = ?", userId, "process").Find(&order)

	c.SetCookie("order_id", strconv.FormatUint(uint64(order.ID), 10), int(time.Hour*24/time.Second), "/", "", false, false)

	c.JSON(http.StatusOK, gin.H{"message": "Cookie has been set!"})
}
