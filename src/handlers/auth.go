package handlers

import (
	"assignment3/src/initializers"
	"assignment3/src/models"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func ShowRegisterPage(c *gin.Context) {
	c.HTML(http.StatusOK, "register.html", gin.H{})
}

func ShowLoginPage(c *gin.Context) {
	c.HTML(http.StatusOK, "login.html", gin.H{})
}

func RegisterHandler(c *gin.Context) {

	firstName := c.PostForm("first_name")
	lastName := c.PostForm("last_name")
	email := c.PostForm("email")
	password := c.PostForm("password")
	confirmPassword := c.PostForm("confirm_password")

	user := &models.User{
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Password:  password,
	}
	err := user.Validate()
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	if password != confirmPassword {
		c.AbortWithError(http.StatusBadRequest, errors.New("passwords do not match"))
		return
	}

	err = user.HashPassword()
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	err = initializers.DB.Create(user).Error
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.Redirect(http.StatusFound, "/login")

}

func LoginHandler(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")

	user := &models.User{
		Email:    email,
		Password: password,
	}
	err := user.Validate()
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var existingUser models.User
	err = initializers.DB.Where("email = ?", email).First(&existingUser).Error
	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, errors.New("invalid email or password"))
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(existingUser.Password), []byte(password))
	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, errors.New("invalid email or password"))
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":   email,
		"user_id": existingUser.ID,
		"exp":     time.Now().Add(time.Hour * 24).Unix(),
	})

	tokenString, err := token.SignedString([]byte("secret"))
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	var order models.Order
	initializers.DB.Where("user_id = ? and status = ?", existingUser.ID, "process").Find(&order)

	if order.ID == 0 {
		order.UserID = int(existingUser.ID)
		order.Status = "process"
		initializers.DB.Create(&order)
	}

	c.SetCookie("order_id", strconv.FormatUint(uint64(order.ID), 10), int(time.Hour*24/time.Second), "/", "", false, false)
	c.SetCookie("token", tokenString, int(time.Hour*24/time.Second), "/", "", false, false)
	c.SetCookie("user_id", strconv.FormatUint(uint64(existingUser.ID), 10), int(time.Hour*24/time.Second), "/", "", false, false)
	c.Redirect(http.StatusFound, "/#")
}

func Logout(c *gin.Context) {
	c.SetCookie("order_id", "", -1, "/", "", false, true)
	c.SetCookie("token", "", -1, "/", "", false, true)
	c.SetCookie("user_id", "", -1, "/", "", false, true)
	c.Redirect(http.StatusSeeOther, "/")
}
