package handlers

import (
	"assignment3/src/initializers"
	"assignment3/src/models"
	"assignment3/src/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func CreateOrder(c *gin.Context) {
	var order models.Order
	if err := c.ShouldBindJSON(&order); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err := initializers.DB.Create(&order).Error

	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusCreated, order)

}

func GetAllOrders(ctx *gin.Context) {
	var orders []models.Order

	err := initializers.DB.Where("status = ?", "process").Find(&orders).Error
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(200, orders)
}

func DeleteOrder(ctx *gin.Context) {
	ID := ctx.Param("id")
	var order models.Order

	initializers.DB.Where("id = ?", ID).Find(&order)

	if order.ID == 0 {
		ctx.JSON(http.StatusNotFound, gin.H{"message": "Order not found!"})
		return
	}

	initializers.DB.Delete(&order)

	ctx.JSON(204, gin.H{"message": "Order deleted!"})
}

func RetrieveOrder(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	var order models.Order
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	initializers.DB.Where("id = ?", id).Find(&order)

	if order.Status == "" {
		ctx.JSON(404, gin.H{"message": "doesn't found"})
		return
	}

	ctx.JSON(200, order)
}

func UpdateOrder(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))

	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var updateOrder models.UpdateOrder

	err = ctx.ShouldBindJSON(&updateOrder)

	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var order models.Order

	err = initializers.DB.Where("id = ?", id).Find(&order).Error

	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var items []models.OrderItem

	err = initializers.DB.Where("order_id = ?", order.ID).Find(&items).Error

	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	order.Status = updateOrder.Status
	order.TotalPrice = utils.CalculateTotalPrice(items)

	initializers.DB.Save(&order)

	ctx.JSON(http.StatusOK, order)

}
