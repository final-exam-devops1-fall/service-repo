package models

import (
	"time"

	"gorm.io/gorm"
)

type Order struct {
	gorm.Model
	UserID     int     `json:"user_id" gorm:"not null"`
	Status     string  `json:"status" gorm:"not null"`
	TotalPrice float64 `json:"total_price" gorm:"not null"`
	Items      []OrderItem
}

type OrderItem struct {
	gorm.Model
	OrderID    int     `json:"order_id" gorm:"not null"`
	ProductID  int     `json:"product_id" gorm:"not null"`
	Quantity   int     `json:"quantity" gorm:"not null"`
	TotalPrice float64 `json:"total_price" gorm:"not null"`
}

type OrderItemInCart struct {
	ProductID   int     `json:"product_id"`
	OrderID     int     `json:"order_id"`
	Quantity    int     `json:"quantity"`
	OrderItemID int     `json:"order_item_id"`
	TotalPrice  float64 `json:"total_price"`
	Name        string  `json:"name"`
	Price       float64 `json:"price"`
	Image       string  `json:"image"`
}

type UpdateOrder struct {
	Status string `json:"status"`
}

type EtlResult struct {
	PurchaseCount int
	TotalAmount   int
	TotalPrice    float64
	CreatedAt     time.Time
}
