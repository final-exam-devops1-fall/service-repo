package initializers

import (
	"assignment3/src/models"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

var DB *gorm.DB
var err error

func ConnectDB() {

	var err error

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s",
		"localhost", "postgres", "postgres", "postgres", "5432")

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Could not connect to the initializers: %v", err)
	}

	DB.AutoMigrate(models.User{}, models.Product{}, models.Order{}, models.OrderItem{})

	log.Println("Connected to the initializers successfully")
}
