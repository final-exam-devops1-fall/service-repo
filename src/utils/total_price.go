package utils

import (
	"assignment3/src/models"
)

func CalculateTotalPrice(orderItems []models.OrderItem) float64 {
	var sum float64

	for _, val := range orderItems {
		sum += val.TotalPrice
	}

	return sum
}
