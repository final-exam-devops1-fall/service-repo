FROM golang:1.21.3-alpine3.17 as builder

WORKDIR /app

COPY . .

RUN go build -o main main.go

FROM alpine:3.14

WORKDIR /app

ENV TZ=Asia/Almaty

ENV LANG=en_US.UTF-8

COPY --from=builder /app/main .

COPY --from=builder /app/src/templates /app/src/templates

COPY .env .

EXPOSE 8080

CMD ["/app/main"]