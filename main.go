package main

import (
	"assignment3/src/initializers"
	"assignment3/src/models"
	"assignment3/src/routes"
	"fmt"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func SetUpRoutes(r *gin.Engine) {
	routes.HtmlPageRoutes(r)
	routes.AuthRoutes(r)
	routes.ProductRoutes(r)
	routes.OrderRoutes(r)
	routes.OrderItemRoutes(r)
}

func main() {
	initializers.LoadConfig()
	initializers.ConnectDB()
	r := gin.Default()
	SetUpRoutes(r)

	go runETL()

	port := ":8080"
	if len(os.Args) > 1 {
		port = ":" + os.Args[1]
	}
	r.Run(port)
}

func runETL() {
	for {
		err := performETL()
		if err != nil {
			fmt.Printf("ETL job error: %v\n", err)
		}
		time.Sleep(1 * time.Minute)
	}
}

func performETL() error {
	now := time.Now()
	oneMinuteAgo := now.Add(-1 * time.Minute)

	var result struct {
		PurchaseCount int
		TotalAmount   int
		TotalPrice    float64
	}

	err := initializers.DB.Table("order_items").
		Select("COUNT(*) AS purchase_count, SUM(quantity) AS total_amount, SUM(total_price) AS total_price").
		Where("created_at >= ? AND created_at <= ?", oneMinuteAgo, now).
		Scan(&result).Error

	if err != nil {
		return err
	}

	fmt.Printf("Last Minute ETL Result: Purchase Count=%d, Total Amount=%d, Total Price=%.2f\n", result.PurchaseCount, result.TotalAmount, result.TotalPrice)

	// Insert the result into the etl_results table
	etlResult := models.EtlResult{
		PurchaseCount: result.PurchaseCount,
		TotalAmount:   result.TotalAmount,
		TotalPrice:    result.TotalPrice,
		CreatedAt:     now,
	}

	if err := initializers.DB.Create(&etlResult).Error; err != nil {
		return err
	}

	return nil
}
